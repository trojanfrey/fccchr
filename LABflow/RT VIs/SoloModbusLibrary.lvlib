﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="404096" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1\404096</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">404096</Property>
	</Item>
	<Item Name="404097" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1\404097</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">404097</Property>
	</Item>
	<Item Name="Modbus1" Type="IO Server">
		<Property Name="atrb" Type="Str">4!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!(%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!!!!!!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!!!!!!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!$AHE!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!"!HU!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!Q&amp;Z!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!1&amp;^!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!U!!!".!']!:!"C!(5!=Q!A!&amp;-!:1"S!'E!91"M!!91!!!%!!!!4A"B!'U!:1!'%!!!"Q!!!%U!&lt;Q"E!')!&gt;1"T!$%!"B!!!!9!!!"1!'%!=A"J!(1!?1!'%!!!"!!!!'5!&gt;A"F!'Y!"B!!!!=!!!"1!'A!&lt;Q"O!'5!4A"P!!91!!!!!!!!"B!!!!A!!!"1!']!&lt;!"M!&amp;)!91"U!'5!!Q!!!!!!!!!!!0!`"B!!!!A!!!"1!()!;1"P!()!;1"U!(E!!Q!&amp;!!!!!!!!!#"!"B!!!!=!!!"3!'5!&gt;!"S!'E!:1"T!!-!"1!!!!!!!!!11!91!!!+!!!!5Q"F!()!;1"B!'Q!5!"P!()!&gt;!!'%!!!"!!!!%-!4Q".!$-!"B!!!!5!!!"4!'M!;1"Q!(-!!Q!&amp;!!!!!!!!!"2!"B!!!!=!!!"4!(1!&lt;Q"Q!%)!;1"U!!91!!!"!!!!-1!'%!!!"Q!!!&amp;1!;1"N!'5!&lt;Q"V!(1!!Q!'!!!!!!!!1(^!"B!!!!E!!!"5!()!91"O!(-!41"P!'1!:1!'%!!!"1!!!%%!5Q"$!%E!31!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="S404096" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1\S404096</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S404096</Property>
	</Item>
	<Item Name="S404097" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1\S404097</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">FCCCHR-cRIO-9024\SoloModbusLibrary.lvlib\Modbus1</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S404097</Property>
	</Item>
</Library>
