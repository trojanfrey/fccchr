﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="subVIs" Type="Folder">
		<Item Name="SHA-1" Type="Folder">
			<Item Name="SHA-1 Core.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/SHA-1/SHA-1 Core.vi"/>
			<Item Name="SHA-1 Digest.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/SHA-1/SHA-1 Digest.vi"/>
			<Item Name="SHA-1 Pad.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/SHA-1/SHA-1 Pad.vi"/>
			<Item Name="SHA-1.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/SHA-1/SHA-1.vi"/>
		</Item>
		<Item Name="lv_mysql_conn_check_packet_type.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_check_packet_type.vi"/>
		<Item Name="lv_mysql_conn_encrypt_passwd.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_encrypt_passwd.vi"/>
		<Item Name="lv_mysql_conn_eof_packet.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_eof_packet.vi"/>
		<Item Name="lv_mysql_conn_init_client_data.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_init_client_data.vi"/>
		<Item Name="lv_mysql_conn_init_server_data.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_init_server_data.vi"/>
		<Item Name="lv_mysql_conn_length_coded_binary.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_length_coded_binary.vi"/>
		<Item Name="lv_mysql_conn_ok_packet.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_ok_packet.vi"/>
		<Item Name="lv_mysql_conn_receive_data_packet.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_receive_data_packet.vi"/>
		<Item Name="lv_mysql_conn_return_packet.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_return_packet.vi"/>
		<Item Name="lv_mysql_conn_send_command.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_send_command.vi"/>
		<Item Name="lv_mysql_conn_send_data_packet.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/subVIs/lv_mysql_conn_send_data_packet.vi"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Item Name="mysql_authentication.ctl" Type="VI" URL="../../lv_mysql_connector_v1_lv8/typedefs/mysql_authentication.ctl"/>
		<Item Name="ok_packet_metadata.ctl" Type="VI" URL="../../lv_mysql_connector_v1_lv8/typedefs/ok_packet_metadata.ctl"/>
		<Item Name="return_packet_type.ctl" Type="VI" URL="../../lv_mysql_connector_v1_lv8/typedefs/return_packet_type.ctl"/>
		<Item Name="server_init_data.ctl" Type="VI" URL="../../lv_mysql_connector_v1_lv8/typedefs/server_init_data.ctl"/>
	</Item>
	<Item Name="LV MySQL Example Equipment Query.vi" Type="VI" URL="../../lv_mysql_connector_v1_lv8/LV MySQL Example Equipment Query.vi"/>
	<Item Name="LV MySQL Example.vi" Type="VI" URL="../lv_mysql_connector_v1_lv8/LV MySQL Example.vi"/>
	<Item Name="lv_mysql_conn_close.vi" Type="VI" URL="../lv_mysql_connector_v1_lv8/lv_mysql_conn_close.vi"/>
	<Item Name="lv_mysql_conn_init.vi" Type="VI" URL="../lv_mysql_connector_v1_lv8/lv_mysql_conn_init.vi"/>
	<Item Name="lv_mysql_conn_query.vi" Type="VI" URL="../lv_mysql_connector_v1_lv8/lv_mysql_conn_query.vi"/>
</Library>
